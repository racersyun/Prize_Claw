#ifndef MYLIB_H_
#define MYLIB_H_

#pragma once
#include "cortex_m4.h"

#define 	Wait_RTC_Control 	while (!(HIBCTL & 0x80000000))

uint32_t g_ui32SysClock;

#define 	HDP_S 	479
#define		VDP_S	271
#define LCD_HEIGHT	272
#define LCD_WIDTH	480
#define SCROLL	10
#define DIV_MS	17

#define LCD_ENTRY_MODE_REG        0x36
#define LCD_RAM_DATA_REG          0x2C
#define LCD_X_RAM_ADDR_REG        0x2A
#define LCD_Y_RAM_ADDR_REG        0x2B

#define DLY_4 	16
#define DLY_8	8
#define DLY_16	4
#define DLY_32	2
#define C1 		61068
#define D1 		54421
#define E1 		48484
#define F1 		45844
#define G1 		40815
#define A1 		36363
#define B1 		32388
#define C2 		30592
#define High	10000
#define NONE	1

#define COLOR_BLUE		0x001f
#define COLOR_GREEN		0x07E0
#define COLOR_RED		0xF800
#define COLOR_WHITE		0xFFFF
#define COLOR_BLACK		0x0000

#define Image_Intro1			0x000000	// IN 480x272
#define Image_Intro2			0x040000	// IN 480x272
#define Image_Intro_100			0x080000	// IN 480x272
#define Image_Intro_200			0x0c0000	// IN 480x272
#define Image_Intro_300			0x100000	// IN 480x272
#define Image_Intro_Start		0x140000	// IN 480x272
#define Image_PlayGround		0x180000	// IN 480x272
#define Image_Product_1F		0x1c0000	// IN 47x85		 Rabbit Crane Grab
#define Image_Product_1U		0x200000	// IN 87x46		 Rabbit Upperside
#define Image_Product_2F		0x240000	// IN 47x85		 MiniCar Crane Grab
#define Image_Product_2U		0x280000	// IN 87x46		 MiniCar Upperside
#define Image_Product_3F		0x2c0000	// IN 47x85		 Pororo Crane Grab
#define Image_Product_3U		0x300000	// IN 87x46		 Pororo Upperside
#define Image_Product_4F		0x340000	// IN 47x85		 Flash Crane Grab
#define Image_Product_4U		0x380000	// IN 87x46		 Flash Upperside
#define Image_Product_5F		0x3c0000	// IN 47x85		 Dino Crane Grab
#define Image_Product_5U		0x400000	// IN 87x46		 Dino Upperside
#define Image_Product_6F		0x440000	// IN 47x85		 RCCar Crane Grab
#define Image_Product_6U		0x480000	// IN 87x46 	 RCCar Upperside
#define Image_Product_7F		0x4c0000	// IN 47x85		 RCHeli Crane Grab
#define Image_Product_7U		0x500000	// IN 87x46 	 RCHeli Upperside
#define Image_Crane_VBar		0x540000	// IN 5x244		 Crane Vertical Bar
#define Image_Crane_HBar		0x580000	// IN 439x5		 Crane Horizontal Bar
#define Image_Crane_Body		0x5c0000	// IN 31x31		 Crane Body
#define Image_Product_Empty		0x600000	// IN 87x46 	 Empty Bucket
#define Image_Grab1				0x640000	// IN 480x272
#define Image_Grab2				0x680000	// IN 480x272
#define Image_Grab_Success1		0x6c0000	// IN 480x272
#define Image_Grab_Success2		0x700000	// IN 480x272
#define Image_Grab_Fail			0x740000	// IN 480x272
#define Image_Ending_Sucess		0x780000	// IN 480x272
#define Image_Ending_Failure	0x7c0000	// IN 480x272
#define Image_Ending_Bye		0x800000	// IN 480x272
#define Image_Product_1D		0x840000	// IN 59x66		 Rabbit Drop
#define Image_Product_2D		0x880000	// IN 59x66		 MiniCar Drop
#define Image_Product_3D		0x8c0000	// IN 59x66		 Pororo Drop
#define Image_Product_4D		0x900000	// IN 59x66		 Flash Drop
#define Image_Product_5D		0x940000	// IN 59x66		 Dino Drop
#define Image_Product_6D		0x980000	// IN 59x66		 RCCar Drop
#define Image_Product_7D		0x9c0000	// IN 59x66		 RCHeli Drop
#define Image_Product_1I		0xa00000	// IN 395x151	 Rabbit Info
#define Image_Product_2I		0xa40000	// IN 395x151	 MiniCar Info
#define Image_Product_3I		0xa80000	// IN 395x151	 Pororo Info
#define Image_Product_4I		0xac0000	// IN 395x151	 Flash Info
#define Image_Product_5I		0xb00000	// IN 395x151	 Dino Info
#define Image_Product_6I		0xb40000	// IN 395x151	 RCCar Info
#define Image_Product_7I		0xb80000	// IN 395x151	 RCHeli Info
#define Image_Product_0I		0xbc0000	// IN 395x151	 Not Grab

static unsigned char fnd_num[18] = {
//	0		1		2		3		4		5		6		7
		0x3f, 0x06, 0x5b, 0x4f, 0x66, 0x6d, 0x7d, 0x27,
//	8		9		a		b		c		d		e		f
		0x7f, 0x67, 0x5f, 0x7c, 0x39, 0x5e, 0x79, 0x71,
//	off		neg
		0x00, 0x40 };

static unsigned char fnd_pos[6] = {
//	D1		D2		D3		D4		D5		D6
		0x01, 0x02, 0x04, 0x08, 0x10, 0x20 };

void LCD_Init(unsigned long ulClockMS);
inline uint16_t ReadData(void);
inline void WriteData(uint16_t ui16Data);
inline void WriteCommand(uint16_t ui8Data);

void Interrupt_Init(void);
void PWM_init(uint32_t ui32SysClock);
void RTC_Init(void);
void PWM0_Interrupt_Enable(void);
void PWM0_Interrupt_Disable(void);

void TIMER_init();
void Push_init();
int PUSH_Check();
void Dip_init();
void LED_init();

void Port_Init(void);

void Uart_init(float BRD, int BRDI, int BRDF);
char Uart_GetCh(void);
char Uart_GetKey(void);
void Uart_PutCh(uint8_t data);
void Uart_PutStr(char* pt);
void Uart_Printf(char *fmt, ...);

void Bluetooth_init(float BRD, int BRDI, int BRDF);
char Bluetooth_GetKey(void);
void Bluetooth_Printf(char *fmt, ...);
void _user_Bluetooth_Interrupt_Handler(void);
void Bluetooth_PutStr(char* pt);
void Bluetooth_PutCh(uint8_t data);

void SetFullFrame();
void PutPixel(unsigned char *buffer, int x, int y, int color);
void delay(int count);

void FND_init();
void WRITE_FND(int digit, int seg_out);
void WRITE_FND_DOT(int digit, int seg_out);
void FND_clear();

void BUZZER_init();
void BUZZER_clear();
void Play(int freq);
void DelayForPlay(unsigned int DLY);

int RTC_Time_Out();
void RTC_Time_Set(int setHour, int setMin, int setSec);

void _user_PWMTimer0Int(void);

void Device_Init();

#endif /* MYLIB_H_ */
