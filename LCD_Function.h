/*
 * LCD_Function.h
 *
 *  Created on: 2015. 6. 1.
 *      Author: Administrator
 */

#ifndef LCD_FUNCTION_H_
#define LCD_FUNCTION_H_

extern uint32_t g_ui32SysClock;

void DrawRect(int x1, int y1, int x2, int y2, int color) {
	int i, j;

	WriteCommand (LCD_X_RAM_ADDR_REG);		// Set X
	WriteData(x1 >> 8);
	WriteData(x1 & 0xFF);
	WriteData(x2 - 1 >> 8);
	WriteData(x2 - 1 & 0xff);

	WriteCommand (LCD_Y_RAM_ADDR_REG);		// Set Y
	WriteData(y1 >> 8);
	WriteData(y1 & 0xFF);
	WriteData(y2 - 1 >> 8);
	WriteData(y2 - 1 & 0xff);

	WriteCommand (LCD_RAM_DATA_REG);

	for (i = y1; i < y2; ++i) {
		for (j = x1; j < x2; ++j) {
			WriteData(color);
		}
	}

	SetFullFrame();
}

void DrawImage(unsigned char *buffer, int x1, int y1, int x2, int y2,
		int image) {
	int i;
	unsigned long ulClockMS;

	ulClockMS = g_ui32SysClock / (3 * 1000);

	WriteCommand (LCD_X_RAM_ADDR_REG);		// Set X
	WriteData(x1 >> 8);
	WriteData(x1 & 0xFF);
	WriteData(x2 - 1 >> 8);
	WriteData(x2 - 1 & 0xff);

	WriteCommand (LCD_Y_RAM_ADDR_REG);		// Set Y
	WriteData(y1 >> 8);
	WriteData(y1 & 0xFF);
	WriteData(y2 - 1 >> 8);
	WriteData(y2 - 1 & 0xff);

	WriteCommand (LCD_RAM_DATA_REG);

	for (i = y1; i <= y2; ++i) {
		MX66L51235FRead(image + ((i - y1) * (x2 - x1) * 2), buffer,
				(x2 - x1) * 2);

		uDMAChannelControlSet(UDMA_CHANNEL_SW | UDMA_PRI_SELECT,
				UDMA_SIZE_16 | UDMA_SRC_INC_16 | UDMA_DST_INC_NONE
						| UDMA_ARB_8);
		uDMAChannelTransferSet(UDMA_CHANNEL_SW | UDMA_PRI_SELECT,
				UDMA_MODE_AUTO, &buffer[0], (void *) 0x44050018, (x2 - x1) / 2);
		uDMAChannelEnable (UDMA_CHANNEL_SW);
		uDMAChannelRequest(UDMA_CHANNEL_SW);
		SysCtlDelay(ulClockMS / DIV_MS);

		uDMAChannelControlSet(UDMA_CHANNEL_SW | UDMA_PRI_SELECT,
				UDMA_SIZE_16 | UDMA_SRC_INC_16 | UDMA_DST_INC_NONE
						| UDMA_ARB_8);
		uDMAChannelTransferSet(UDMA_CHANNEL_SW | UDMA_PRI_SELECT,
				UDMA_MODE_AUTO, &buffer[(x2 - x1)], (void *) 0x44050018,
				(x2 - x1) / 2);
		uDMAChannelEnable(UDMA_CHANNEL_SW);
		uDMAChannelRequest(UDMA_CHANNEL_SW);
		SysCtlDelay(ulClockMS / DIV_MS);
	}

	SetFullFrame();
}
void RestoreBackground(unsigned char *buffer, int x1, int y1, int x2, int y2,
		int image) { // maximum 300 pixel
	int i;
	unsigned long ulClockMS;

	ulClockMS = g_ui32SysClock / (3 * 1000);

	WriteCommand (LCD_X_RAM_ADDR_REG);		// Set X
	WriteData(x1 >> 8);
	WriteData(x1 & 0xFF);
	WriteData(x2 - 1 >> 8);
	WriteData(x2 - 1 & 0xff);

	WriteCommand (LCD_Y_RAM_ADDR_REG);		// Set Y
	WriteData(y1 >> 8);
	WriteData(y1 & 0xFF);
	WriteData(y2 - 1 >> 8);
	WriteData(y2 - 1 & 0xff);

	WriteCommand (LCD_RAM_DATA_REG);

	for (i = y1; i <= y2; ++i) {
		MX66L51235FRead(image + (i * 480 * 2 + x1 * 2), buffer, (x2 - x1) * 2);

		uDMAChannelControlSet(UDMA_CHANNEL_SW | UDMA_PRI_SELECT,
				UDMA_SIZE_16 | UDMA_SRC_INC_16 | UDMA_DST_INC_NONE
						| UDMA_ARB_8);
		uDMAChannelTransferSet(UDMA_CHANNEL_SW | UDMA_PRI_SELECT,
				UDMA_MODE_AUTO, &buffer[0], (void *) 0x44050018, (x2 - x1));
		uDMAChannelEnable (UDMA_CHANNEL_SW);
		uDMAChannelRequest(UDMA_CHANNEL_SW);
		SysCtlDelay(ulClockMS / DIV_MS);
	}

	SetFullFrame();
}

void DrawCircle(unsigned char *buffer, int nCenterX, int nCenterY, int nRadius,
		int color) {
	int x, y;

	for (x = nCenterX - nRadius; x < nCenterX + nRadius; x++)
		for (y = nCenterY - nRadius; y < nCenterY + nRadius; y++)
			if (((nCenterX - x) * (nCenterX - x))
					+ ((nCenterY - y) * (nCenterY - y)) <= nRadius * nRadius
					&& ((nCenterX - x) * (nCenterX - x))
							+ ((nCenterY - y) * (nCenterY - y))
							>= ((nRadius - nRadius) * (nRadius - nRadius)))	//((nRadius-1) * (nRadius-1)) : 테두리 있는 원(채우기X)
				PutPixel(buffer, x, y, color);

	SetFullFrame();
}

void DrawCircleLine(unsigned char *buffer, int nCenterX, int nCenterY,
		int nRadius, int color) {
	int x, y;

	for (x = nCenterX - nRadius; x < nCenterX + nRadius; x++)
		for (y = nCenterY - nRadius; y < nCenterY + nRadius; y++)
			if (((nCenterX - x) * (nCenterX - x))
					+ ((nCenterY - y) * (nCenterY - y)) <= nRadius * nRadius
					&& ((nCenterX - x) * (nCenterX - x))
							+ ((nCenterY - y) * (nCenterY - y))
							>= ((nRadius - 3) * (nRadius - 3)))	//((nRadius-1) * (nRadius-1)) : 테두리 있는 원(채우기X)
				PutPixel(buffer, x, y, color);

	SetFullFrame();
}

void PutPixel(unsigned char *buffer, int x, int y, int color) {
	WriteCommand (LCD_X_RAM_ADDR_REG);		// Set X
	WriteData(x >> 8);
	WriteData(x & 0xFF);
	WriteData(x >> 8);
	WriteData(x & 0xff);

	WriteCommand (LCD_Y_RAM_ADDR_REG);		// Set Y
	WriteData(y >> 8);
	WriteData(y & 0xFF);
	WriteData(y >> 8);
	WriteData(y & 0xff);

	WriteCommand (LCD_RAM_DATA_REG);
	WriteData(color);
}

void DrawLine(unsigned char *buffer, int x1, int y1, int x2, int y2,
		int color) {
	int dx, dy, e;
	dx = x2 - x1;
	dy = y2 - y1;

	if (dx >= 0) {
		if (dy >= 0) // dy>=0
				{
			if (dx >= dy) // 1/8 octant
					{
				e = dy - dx / 2;
				while (x1 <= x2) {
					PutPixel(buffer, x1, y1, color);

					if (e > 0) {
						y1 += 1;
						e -= dx;
					}
					x1 += 1;
					e += dy;
				}
			} else		// 2/8 octant
			{
				e = dx - dy / 2;
				while (y1 <= y2) {
					PutPixel(buffer, x1, y1, color);

					if (e > 0) {
						x1 += 1;
						e -= dy;
					}
					y1 += 1;
					e += dx;
				}
			}
		} else		   // dy<0
		{
			dy = -dy;   // dy=abs(dy)

			if (dx >= dy) // 8/8 octant
					{
				e = dy - dx / 2;
				while (x1 <= x2) {
					PutPixel(buffer, x1, y1, color);

					if (e > 0) {
						y1 -= 1;
						e -= dx;
					}
					x1 += 1;
					e += dy;
				}
			} else		// 7/8 octant
			{
				e = dx - dy / 2;
				while (y1 >= y2) {
					PutPixel(buffer, x1, y1, color);

					if (e > 0) {
						x1 += 1;
						e -= dy;
					}
					y1 -= 1;
					e += dx;
				}
			}
		}
	} else //dx<0
	{
		dx = -dx;		//dx=abs(dx)
		if (dy >= 0) // dy>=0
				{
			if (dx >= dy) // 4/8 octant
					{
				e = dy - dx / 2;
				while (x1 >= x2) {
					PutPixel(buffer, x1, y1, color);

					if (e > 0) {
						y1 += 1;
						e -= dx;
					}
					x1 -= 1;
					e += dy;
				}
			} else		// 3/8 octant
			{
				e = dx - dy / 2;
				while (y1 <= y2) {
					PutPixel(buffer, x1, y1, color);

					if (e > 0) {
						x1 -= 1;
						e -= dy;
					}
					y1 += 1;
					e += dx;
				}
			}
		} else		   // dy<0
		{
			dy = -dy;   // dy=abs(dy)

			if (dx >= dy) // 5/8 octant
					{
				e = dy - dx / 2;
				while (x1 >= x2) {
					PutPixel(buffer, x1, y1, color);

					if (e > 0) {
						y1 -= 1;
						e -= dx;
					}
					x1 -= 1;
					e += dy;
				}
			} else		// 6/8 octant
			{
				e = dx - dy / 2;
				while (y1 >= y2) {
					PutPixel(buffer, x1, y1, color);

					if (e > 0) {
						x1 -= 1;
						e -= dy;
					}
					y1 -= 1;
					e += dx;
				}
			}
		}
	}

	SetFullFrame();
}

void Scroll_left(unsigned char *buffer, int x1, int y1, int x2, int y2,
		int scroll) {
	int i, j;
	static unsigned char temp_buffer[LCD_HEIGHT * 20];
	unsigned long ulClockMS = g_ui32SysClock / (3 * 1000);

	WriteCommand (LCD_Y_RAM_ADDR_REG);
	WriteData(y1 >> 8);
	WriteData(y1 & 0xFF);
	WriteData(y2 - 1 >> 8);
	WriteData(y2 - 1 & 0xff);

	for (j = x1; j <= x2; j += scroll) {
		if (j != x2) {
			WriteCommand (LCD_X_RAM_ADDR_REG);
			WriteData(j >> 8);
			WriteData(j & 0xFF);
			WriteData((j + scroll - 1) >> 8);
			WriteData((j + scroll - 1) & 0xff);

			WriteCommand(0x2E);
			for (i = 0; i < scroll; i++) {
				uDMAChannelControlSet(UDMA_CHANNEL_SW | UDMA_PRI_SELECT,
						UDMA_SIZE_16 | UDMA_SRC_INC_NONE | UDMA_DST_INC_16
								| UDMA_ARB_8);
				uDMAChannelTransferSet(UDMA_CHANNEL_SW | UDMA_PRI_SELECT,
						UDMA_MODE_AUTO, (void *) 0x44050018,
						&buffer[i * (y2 - y1) * 2], (y2 - y1));
				uDMAChannelEnable (UDMA_CHANNEL_SW);
				uDMAChannelRequest(UDMA_CHANNEL_SW);
				SysCtlDelay(ulClockMS / DIV_MS);
			}
		}

		WriteCommand (LCD_X_RAM_ADDR_REG);
		if (j >= x2) {
			WriteData((j - scroll) >> 8);
			WriteData((j - scroll) & 0xFF);
			WriteData((j - 1) >> 8);
			WriteData((j - 1) & 0xff);

			WriteCommand (LCD_RAM_DATA_REG);
			for (i = 0; i < scroll; i++) {
				uDMAChannelControlSet(UDMA_CHANNEL_SW | UDMA_PRI_SELECT,
						UDMA_SIZE_16 | UDMA_SRC_INC_16 | UDMA_DST_INC_NONE
								| UDMA_ARB_8);
				uDMAChannelTransferSet(UDMA_CHANNEL_SW | UDMA_PRI_SELECT,
						UDMA_MODE_AUTO, &temp_buffer[i * (y2 - y1) * 2],
						(void *) 0x44050018, (y2 - y1));
				uDMAChannelEnable (UDMA_CHANNEL_SW);
				uDMAChannelRequest(UDMA_CHANNEL_SW);
				SysCtlDelay(ulClockMS / DIV_MS);
			}
		}

		else if (j > x1) {
			WriteData((j - scroll) >> 8);
			WriteData((j - scroll) & 0xFF);
			WriteData((j - 1) >> 8);
			WriteData((j - 1) & 0xff);

			WriteCommand (LCD_RAM_DATA_REG);
			for (i = 0; i < scroll; i++) {
				uDMAChannelControlSet(UDMA_CHANNEL_SW | UDMA_PRI_SELECT,
						UDMA_SIZE_16 | UDMA_SRC_INC_16 | UDMA_DST_INC_NONE
								| UDMA_ARB_8);
				uDMAChannelTransferSet(UDMA_CHANNEL_SW | UDMA_PRI_SELECT,
						UDMA_MODE_AUTO, &buffer[i * (y2 - y1) * 2],
						(void *) 0x44050018, (y2 - y1));
				uDMAChannelEnable (UDMA_CHANNEL_SW);
				uDMAChannelRequest(UDMA_CHANNEL_SW);
				SysCtlDelay(ulClockMS / DIV_MS);
			}

		}

		else if (j == x1) {
			for (i = 0; i < scroll; i++) {
				uDMAChannelControlSet(UDMA_CHANNEL_SW | UDMA_PRI_SELECT,
						UDMA_SIZE_16 | UDMA_SRC_INC_16 | UDMA_DST_INC_16
								| UDMA_ARB_8);
				uDMAChannelTransferSet(UDMA_CHANNEL_SW | UDMA_PRI_SELECT,
						UDMA_MODE_AUTO, &buffer[i * (y2 - y1) * 2],
						&temp_buffer[i * (y2 - y1) * 2], (y2 - y1));
				uDMAChannelEnable (UDMA_CHANNEL_SW);
				uDMAChannelRequest(UDMA_CHANNEL_SW);
				SysCtlDelay(ulClockMS / DIV_MS);
			}
		}
	}

	SetFullFrame();
}

void Scroll_right(unsigned char *buffer, int x1, int y1, int x2, int y2,
		int scroll) {
	int i, j;
	static unsigned char temp_buffer[LCD_HEIGHT * 20];
	unsigned long ulClockMS = g_ui32SysClock / (3 * 1000);

	WriteCommand (LCD_Y_RAM_ADDR_REG);
	WriteData(y1 >> 8);
	WriteData(y1 & 0xFF);
	WriteData(y2 - 1 >> 8);
	WriteData(y2 - 1 & 0xff);

	for (j = x2; j >= x1; j -= scroll) {
		if (j != x1) {
			WriteCommand (LCD_X_RAM_ADDR_REG);
			WriteData((j - scroll) >> 8);
			WriteData((j - scroll) & 0xFF);
			WriteData((j - 1) >> 8);
			WriteData((j - 1) & 0xff);

			WriteCommand(0x2E);
			for (i = 0; i < scroll; i++) {
				uDMAChannelControlSet(UDMA_CHANNEL_SW | UDMA_PRI_SELECT,
						UDMA_SIZE_16 | UDMA_SRC_INC_NONE | UDMA_DST_INC_16
								| UDMA_ARB_8);
				uDMAChannelTransferSet(UDMA_CHANNEL_SW | UDMA_PRI_SELECT,
						UDMA_MODE_AUTO, (void *) 0x44050018,
						&buffer[i * (y2 - y1) * 2], (y2 - y1));
				uDMAChannelEnable (UDMA_CHANNEL_SW);
				uDMAChannelRequest(UDMA_CHANNEL_SW);
				SysCtlDelay(ulClockMS / DIV_MS);
			}
		}

		if (j == x2) {
			for (i = 0; i < scroll; i++) {
				uDMAChannelControlSet(UDMA_CHANNEL_SW | UDMA_PRI_SELECT,
						UDMA_SIZE_16 | UDMA_SRC_INC_16 | UDMA_DST_INC_16
								| UDMA_ARB_8);
				uDMAChannelTransferSet(UDMA_CHANNEL_SW | UDMA_PRI_SELECT,
						UDMA_MODE_AUTO, &buffer[i * (y2 - y1) * 2],
						&temp_buffer[i * (y2 - y1) * 2], (y2 - y1));
				uDMAChannelEnable (UDMA_CHANNEL_SW);
				uDMAChannelRequest(UDMA_CHANNEL_SW);
				SysCtlDelay(ulClockMS / DIV_MS);
			}
		} else if (j == x1) {
			WriteCommand (LCD_X_RAM_ADDR_REG);
			WriteData(j >> 8);
			WriteData(j & 0xFF);
			WriteData((j + scroll - 1) >> 8);
			WriteData((j + scroll - 1) & 0xff);

			WriteCommand (LCD_RAM_DATA_REG);
			for (i = 0; i < scroll; i++) {
				uDMAChannelControlSet(UDMA_CHANNEL_SW | UDMA_PRI_SELECT,
						UDMA_SIZE_16 | UDMA_SRC_INC_16 | UDMA_DST_INC_NONE
								| UDMA_ARB_8);
				uDMAChannelTransferSet(UDMA_CHANNEL_SW | UDMA_PRI_SELECT,
						UDMA_MODE_AUTO, &temp_buffer[i * (y2 - y1) * 2],
						(void *) 0x44050018, (y2 - y1));
				uDMAChannelEnable (UDMA_CHANNEL_SW);
				uDMAChannelRequest(UDMA_CHANNEL_SW);
				SysCtlDelay(ulClockMS / DIV_MS);
			}
		} else if (j < x2) {
			WriteCommand (LCD_X_RAM_ADDR_REG);
			WriteData(j >> 8);
			WriteData(j & 0xFF);
			WriteData((j + scroll - 1) >> 8);
			WriteData((j + scroll - 1) & 0xff);

			WriteCommand (LCD_RAM_DATA_REG);
			for (i = 0; i < scroll; i++) {
				uDMAChannelControlSet(UDMA_CHANNEL_SW | UDMA_PRI_SELECT,
						UDMA_SIZE_16 | UDMA_SRC_INC_16 | UDMA_DST_INC_NONE
								| UDMA_ARB_8);
				uDMAChannelTransferSet(UDMA_CHANNEL_SW | UDMA_PRI_SELECT,
						UDMA_MODE_AUTO, &buffer[i * (y2 - y1) * 2],
						(void *) 0x44050018, (y2 - y1));
				uDMAChannelEnable (UDMA_CHANNEL_SW);
				uDMAChannelRequest(UDMA_CHANNEL_SW);
				SysCtlDelay(ulClockMS / DIV_MS);
			}
		}
	}

	SetFullFrame();
}
void Scroll_down(unsigned char *buffer, int x1, int y1, int x2, int y2,
		int scroll) {
	int i, j;
	static unsigned char temp_buffer[LCD_WIDTH * 20];
	unsigned long ulClockMS = g_ui32SysClock / (3 * 1000);

	WriteCommand (LCD_X_RAM_ADDR_REG);
	WriteData(x1 >> 8);
	WriteData(x1 & 0xFF);
	WriteData(x2 - 1 >> 8);
	WriteData(x2 - 1 & 0xff);

	for (j = y1; j <= y2; j += scroll) {
		if (j != y2) {
			WriteCommand (LCD_Y_RAM_ADDR_REG);
			WriteData(j >> 8);
			WriteData(j & 0xFF);
			WriteData((j + scroll - 1) >> 8);
			WriteData((j + scroll - 1) & 0xff);

			WriteCommand(0x2E);
			for (i = 0; i < scroll; i++) {
				uDMAChannelControlSet(UDMA_CHANNEL_SW | UDMA_PRI_SELECT,
						UDMA_SIZE_16 | UDMA_SRC_INC_NONE | UDMA_DST_INC_16
								| UDMA_ARB_8);
				uDMAChannelTransferSet(UDMA_CHANNEL_SW | UDMA_PRI_SELECT,
						UDMA_MODE_AUTO, (void *) 0x44050018,
						&buffer[(i * 2) * (x2 - x1)], (x2 - x1) / 2);
				uDMAChannelEnable (UDMA_CHANNEL_SW);
				uDMAChannelRequest(UDMA_CHANNEL_SW);
				SysCtlDelay(ulClockMS / DIV_MS);

				uDMAChannelControlSet(UDMA_CHANNEL_SW | UDMA_PRI_SELECT,
						UDMA_SIZE_16 | UDMA_SRC_INC_NONE | UDMA_DST_INC_16
								| UDMA_ARB_8);
				uDMAChannelTransferSet(UDMA_CHANNEL_SW | UDMA_PRI_SELECT,
						UDMA_MODE_AUTO, (void *) 0x44050018,
						&buffer[(i * 2 + 1) * (x2 - x1)], (x2 - x1) / 2);
				uDMAChannelEnable(UDMA_CHANNEL_SW);
				uDMAChannelRequest(UDMA_CHANNEL_SW);
				SysCtlDelay(ulClockMS / DIV_MS);
			}
		}

		WriteCommand (LCD_Y_RAM_ADDR_REG);
		if (j >= y2) {
			WriteData((j - scroll) >> 8);
			WriteData((j - scroll) & 0xFF);
			WriteData((j - 1) >> 8);
			WriteData((j - 1) & 0xff);

			WriteCommand (LCD_RAM_DATA_REG);
			for (i = 0; i < scroll; i++) {
				uDMAChannelControlSet(UDMA_CHANNEL_SW | UDMA_PRI_SELECT,
						UDMA_SIZE_16 | UDMA_SRC_INC_16 | UDMA_DST_INC_NONE
								| UDMA_ARB_8);
				uDMAChannelTransferSet(UDMA_CHANNEL_SW | UDMA_PRI_SELECT,
						UDMA_MODE_AUTO, &temp_buffer[(i * 2) * (x2 - x1)],
						(void *) 0x44050018, (x2 - x1) / 2);
				uDMAChannelEnable (UDMA_CHANNEL_SW);
				uDMAChannelRequest(UDMA_CHANNEL_SW);
				SysCtlDelay(ulClockMS / DIV_MS);

				uDMAChannelControlSet(UDMA_CHANNEL_SW | UDMA_PRI_SELECT,
						UDMA_SIZE_16 | UDMA_SRC_INC_16 | UDMA_DST_INC_NONE
								| UDMA_ARB_8);
				uDMAChannelTransferSet(UDMA_CHANNEL_SW | UDMA_PRI_SELECT,
						UDMA_MODE_AUTO, &temp_buffer[(i * 2 + 1) * (x2 - x1)],
						(void *) 0x44050018, (x2 - x1) / 2);
				uDMAChannelEnable(UDMA_CHANNEL_SW);
				uDMAChannelRequest(UDMA_CHANNEL_SW);
				SysCtlDelay(ulClockMS / DIV_MS);
			}
		}

		else if (j > y1) {
			WriteData((j - scroll) >> 8);
			WriteData((j - scroll) & 0xFF);
			WriteData((j - 1) >> 8);
			WriteData((j - 1) & 0xff);

			WriteCommand (LCD_RAM_DATA_REG);
			for (i = 0; i < scroll; i++) {
				uDMAChannelControlSet(UDMA_CHANNEL_SW | UDMA_PRI_SELECT,
						UDMA_SIZE_16 | UDMA_SRC_INC_16 | UDMA_DST_INC_NONE
								| UDMA_ARB_8);
				uDMAChannelTransferSet(UDMA_CHANNEL_SW | UDMA_PRI_SELECT,
						UDMA_MODE_AUTO, &buffer[(i * 2) * (x2 - x1)],
						(void *) 0x44050018, (x2 - x1) / 2);
				uDMAChannelEnable (UDMA_CHANNEL_SW);
				uDMAChannelRequest(UDMA_CHANNEL_SW);
				SysCtlDelay(ulClockMS / DIV_MS);

				uDMAChannelControlSet(UDMA_CHANNEL_SW | UDMA_PRI_SELECT,
						UDMA_SIZE_16 | UDMA_SRC_INC_16 | UDMA_DST_INC_NONE
								| UDMA_ARB_8);
				uDMAChannelTransferSet(UDMA_CHANNEL_SW | UDMA_PRI_SELECT,
						UDMA_MODE_AUTO, &buffer[(i * 2 + 1) * (x2 - x1)],
						(void *) 0x44050018, (x2 - x1) / 2);
				uDMAChannelEnable(UDMA_CHANNEL_SW);
				uDMAChannelRequest(UDMA_CHANNEL_SW);
				SysCtlDelay(ulClockMS / DIV_MS);
			}

		}

		else if (j == y1) {
			for (i = 0; i < scroll; i++) {
				uDMAChannelControlSet(UDMA_CHANNEL_SW | UDMA_PRI_SELECT,
						UDMA_SIZE_16 | UDMA_SRC_INC_16 | UDMA_DST_INC_16
								| UDMA_ARB_8);
				uDMAChannelTransferSet(UDMA_CHANNEL_SW | UDMA_PRI_SELECT,
						UDMA_MODE_AUTO, &buffer[(i * 2) * (x2 - x1)],
						&temp_buffer[(i * 2) * (x2 - x1)], (x2 - x1) / 2);
				uDMAChannelEnable (UDMA_CHANNEL_SW);
				uDMAChannelRequest(UDMA_CHANNEL_SW);
				SysCtlDelay(ulClockMS / DIV_MS);

				uDMAChannelControlSet(UDMA_CHANNEL_SW | UDMA_PRI_SELECT,
						UDMA_SIZE_16 | UDMA_SRC_INC_16 | UDMA_DST_INC_16
								| UDMA_ARB_8);
				uDMAChannelTransferSet(UDMA_CHANNEL_SW | UDMA_PRI_SELECT,
						UDMA_MODE_AUTO, &buffer[(i * 2 + 1) * (x2 - x1)],
						&temp_buffer[(i * 2 + 1) * (x2 - x1)], (x2 - x1) / 2);
				uDMAChannelEnable(UDMA_CHANNEL_SW);
				uDMAChannelRequest(UDMA_CHANNEL_SW);
				SysCtlDelay(ulClockMS / DIV_MS);
			}
		}
	}

	SetFullFrame();
}

void Scroll_up(unsigned char *buffer, int x1, int y1, int x2, int y2,
		int scroll) {
	int i, j;
	static unsigned char temp_buffer[LCD_WIDTH * 20];
	unsigned long ulClockMS = g_ui32SysClock / (3 * 1000);

	WriteCommand (LCD_X_RAM_ADDR_REG);
	WriteData(x1 >> 8);
	WriteData(x1 & 0xFF);
	WriteData(x2 - 1 >> 8);
	WriteData(x2 - 1 & 0xff);

	for (j = y2; j >= y1; j -= scroll) {
		if (j != y1) {
			WriteCommand (LCD_Y_RAM_ADDR_REG);
			WriteData((j - scroll) >> 8);
			WriteData((j - scroll) & 0xFF);
			WriteData((j - 1) >> 8);
			WriteData((j - 1) & 0xff);

			WriteCommand(0x2E);
			for (i = 0; i < scroll; i++) {
				uDMAChannelControlSet(UDMA_CHANNEL_SW | UDMA_PRI_SELECT,
						UDMA_SIZE_16 | UDMA_SRC_INC_NONE | UDMA_DST_INC_16
								| UDMA_ARB_8);
				uDMAChannelTransferSet(UDMA_CHANNEL_SW | UDMA_PRI_SELECT,
						UDMA_MODE_AUTO, (void *) 0x44050018,
						&buffer[(i * 2) * (x2 - x1)], (x2 - x1) / 2);
				uDMAChannelEnable (UDMA_CHANNEL_SW);
				uDMAChannelRequest(UDMA_CHANNEL_SW);
				SysCtlDelay(ulClockMS / DIV_MS);

				uDMAChannelControlSet(UDMA_CHANNEL_SW | UDMA_PRI_SELECT,
						UDMA_SIZE_16 | UDMA_SRC_INC_NONE | UDMA_DST_INC_16
								| UDMA_ARB_8);
				uDMAChannelTransferSet(UDMA_CHANNEL_SW | UDMA_PRI_SELECT,
						UDMA_MODE_AUTO, (void *) 0x44050018,
						&buffer[(i * 2 + 1) * (x2 - x1)], (x2 - x1) / 2);
				uDMAChannelEnable(UDMA_CHANNEL_SW);
				uDMAChannelRequest(UDMA_CHANNEL_SW);
				SysCtlDelay(ulClockMS / DIV_MS);
			}
		}

		WriteCommand (LCD_Y_RAM_ADDR_REG);
		if (j == y2) {
			for (i = 0; i < scroll; i++) {
				uDMAChannelControlSet(UDMA_CHANNEL_SW | UDMA_PRI_SELECT,
						UDMA_SIZE_16 | UDMA_SRC_INC_16 | UDMA_DST_INC_16
								| UDMA_ARB_8);
				uDMAChannelTransferSet(UDMA_CHANNEL_SW | UDMA_PRI_SELECT,
						UDMA_MODE_AUTO, &buffer[(i * 2) * (x2 - x1)],
						&temp_buffer[(i * 2) * (x2 - x1)], (x2 - x1) / 2);
				uDMAChannelEnable (UDMA_CHANNEL_SW);
				uDMAChannelRequest(UDMA_CHANNEL_SW);
				SysCtlDelay(ulClockMS / DIV_MS);

				uDMAChannelControlSet(UDMA_CHANNEL_SW | UDMA_PRI_SELECT,
						UDMA_SIZE_16 | UDMA_SRC_INC_16 | UDMA_DST_INC_16
								| UDMA_ARB_8);
				uDMAChannelTransferSet(UDMA_CHANNEL_SW | UDMA_PRI_SELECT,
						UDMA_MODE_AUTO, &buffer[(i * 2 + 1) * (x2 - x1)],
						&temp_buffer[(i * 2 + 1) * (x2 - x1)], (x2 - x1) / 2);
				uDMAChannelEnable(UDMA_CHANNEL_SW);
				uDMAChannelRequest(UDMA_CHANNEL_SW);
				SysCtlDelay(ulClockMS / DIV_MS);
			}
		} else if (j <= y1) {
			WriteData(j >> 8);
			WriteData(j & 0xFF);
			WriteData((j + scroll - 1) >> 8);
			WriteData((j + scroll - 1) & 0xff);

			WriteCommand (LCD_RAM_DATA_REG);
			for (i = 0; i < scroll; i++) {
				uDMAChannelControlSet(UDMA_CHANNEL_SW | UDMA_PRI_SELECT,
						UDMA_SIZE_16 | UDMA_SRC_INC_16 | UDMA_DST_INC_NONE
								| UDMA_ARB_8);
				uDMAChannelTransferSet(UDMA_CHANNEL_SW | UDMA_PRI_SELECT,
						UDMA_MODE_AUTO, &temp_buffer[(i * 2) * (x2 - x1)],
						(void *) 0x44050018, (x2 - x1) / 2);
				uDMAChannelEnable (UDMA_CHANNEL_SW);
				uDMAChannelRequest(UDMA_CHANNEL_SW);
				SysCtlDelay(ulClockMS / DIV_MS);

				uDMAChannelControlSet(UDMA_CHANNEL_SW | UDMA_PRI_SELECT,
						UDMA_SIZE_16 | UDMA_SRC_INC_16 | UDMA_DST_INC_NONE
								| UDMA_ARB_8);
				uDMAChannelTransferSet(UDMA_CHANNEL_SW | UDMA_PRI_SELECT,
						UDMA_MODE_AUTO, &temp_buffer[(i * 2 + 1) * (x2 - x1)],
						(void *) 0x44050018, (x2 - x1) / 2);
				uDMAChannelEnable(UDMA_CHANNEL_SW);
				uDMAChannelRequest(UDMA_CHANNEL_SW);
				SysCtlDelay(ulClockMS / DIV_MS);
			}
		} else if (j) {
			WriteData(j >> 8);
			WriteData(j & 0xFF);
			WriteData((j + scroll - 1) >> 8);
			WriteData((j + scroll - 1) & 0xff);

			WriteCommand (LCD_RAM_DATA_REG);
			for (i = 0; i < scroll; i++) {
				uDMAChannelControlSet(UDMA_CHANNEL_SW | UDMA_PRI_SELECT,
						UDMA_SIZE_16 | UDMA_SRC_INC_16 | UDMA_DST_INC_NONE
								| UDMA_ARB_8);
				uDMAChannelTransferSet(UDMA_CHANNEL_SW | UDMA_PRI_SELECT,
						UDMA_MODE_AUTO, &buffer[(i * 2) * (x2 - x1)],
						(void *) 0x44050018, (x2 - x1) / 2);
				uDMAChannelEnable (UDMA_CHANNEL_SW);
				uDMAChannelRequest(UDMA_CHANNEL_SW);
				SysCtlDelay(ulClockMS / DIV_MS);

				uDMAChannelControlSet(UDMA_CHANNEL_SW | UDMA_PRI_SELECT,
						UDMA_SIZE_16 | UDMA_SRC_INC_16 | UDMA_DST_INC_NONE
								| UDMA_ARB_8);
				uDMAChannelTransferSet(UDMA_CHANNEL_SW | UDMA_PRI_SELECT,
						UDMA_MODE_AUTO, &buffer[(i * 2 + 1) * (x2 - x1)],
						(void *) 0x44050018, (x2 - x1) / 2);
				uDMAChannelEnable(UDMA_CHANNEL_SW);
				uDMAChannelRequest(UDMA_CHANNEL_SW);
				SysCtlDelay(ulClockMS / DIV_MS);
			}

		}
	}

	SetFullFrame();
}

void SetFullFrame() {
	WriteCommand (LCD_X_RAM_ADDR_REG);
	WriteData(0);
	WriteData(0);
	WriteData(479 >> 8);
	WriteData(479 & 0xff);

	WriteCommand (LCD_Y_RAM_ADDR_REG);
	WriteData(0 >> 8);
	WriteData(0 & 0xFF);
	WriteData(271 >> 8);
	WriteData(271 & 0xff);
}

#endif /* LCD_FUNCTION_H_ */
