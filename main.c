#include "cortex_m4.h"
#include "myLib.h"
#include "math.h"
#include "time.h"
#include "LCD_Function.h"

unsigned char buffer[LCD_WIDTH * LCD_HEIGHT];
int32_t PointerMessage(uint32_t ui32Message, int32_t i32X, int32_t i32Y);
extern int32_t user_X, user_Y;

void Game_Start();			// 게임시작
void Intro_Play();			// 초기 화면 및 동전 입력
void Game_Play();			// 본 게임 구동
void Game_ProductSet();		// 플레이 구간에 물품 배치
void Game_CraneMove();		// 키 입력으로 크레인 이동
void Game_CraneGrabPoint();	// 크레인 포인트 위치 아이템 식별
int Game_Grab_Calculate(int difficulty);	// 확률 계산
void Game_CraneGrab();		// 크레인 무빙 애니메이션
void Ending_Play();			// 물품 정보 및 엔딩

void BUZ_Coin_Add();		// 동전 추가
void BUZ_Coin_Sub();		// 동전 반환
void BUZ_Move_Confirm();	// 이동 확인
void BUZ_Crane_Down();		// 크레인 하강
void BUZ_Crane_Release();	// 크레인 집게 해제
void BUZ_Success();			// 획득 성공
void BUZ_Failure();			// 획득 실패

int Coin = 0;				// 동전 투입 최대 3
int Start_Flag = 0;			// 동전이 투입된 후 게임시작 터치하면 플래그=1
int CircleLine_Flag = 0;	// 목표 지점 설정
int Crane_Move_Flag = 0;	// 크레인 종/횡 움직임 설정
int Crane_Sucess_Flag = 0;	// 0=실패 1=성공
int Crane_Grab_Flag = 0;

int Item_Sequence[12] = { 0 };	// 물품 배치 순서

int Bx1 = 26, By1 = 20, Bx2 = 57, By2 = 51;
int Selected_Item = 0;
int Play_Time = 8;
int CircleX = 0, CircleY = 0;
uint8_t code;

struct Product {
	char name[25];
	char difficulty[25];
	int price;
	int success_rate;
	int up_image;
	int side_image;
	int drop_image;
	int info_image;
};

struct Product product[8] = { { "Can't Grab\r\n", "", 0, 0, Image_Product_Empty,
Image_Product_Empty, Image_Product_Empty, Image_Product_0I }, {
		"Name:Rabbit\r\n", "Level:Easy\r\n", 50, 100,
		Image_Product_1U,
		Image_Product_1F, Image_Product_1D, Image_Product_1I }, {
		"Name:MiniCar\r\n", "Level:Easy\r\n", 70, 90,
		Image_Product_2U,
		Image_Product_2F, Image_Product_2D, Image_Product_2I }, {
		"Name:Pororo\r\n", "Level:Easy\r\n", 10000, 80,
		Image_Product_3U,
		Image_Product_3F, Image_Product_3D, Image_Product_3I }, {
		"Name:Flashlight\r\n", "Level:Medium\r\n", 8000, 70,
		Image_Product_4U, Image_Product_4F, Image_Product_4D,
		Image_Product_4I }, { "Name:Dinosaur\r\n", "Level:Medium\r\n", 9000, 60,
Image_Product_5U, Image_Product_5F, Image_Product_5D,
Image_Product_5I }, { "Name:RC Car\r\n", "Level:Hard\r\n", 30000, 30,
Image_Product_6U, Image_Product_6F,
Image_Product_6D, Image_Product_6I }, { "Name:RC Helicopter\r\n",
		"Level:Extreme\r\n", 60000, 20,
		Image_Product_7U,
		Image_Product_7F, Image_Product_7D, Image_Product_7I } };

int main(void) {
	// Run from the PLL at 120 MHz.
	g_ui32SysClock = SysCtlClockFreqSet((SYSCTL_XTAL_25MHZ |
	SYSCTL_OSC_MAIN | SYSCTL_USE_PLL |
	SYSCTL_CFG_VCO_480), 120000000);

	Device_Init();
	FND_clear();
	BUZZER_clear();

	TouchScreenInit(g_ui32SysClock);
	TouchScreenCallbackSet(PointerMessage);

	GPTMCTL = GPTMCTL | 0x41;   		 // timer enable

	Game_Start();
}

void Game_Start() {
	while (Coin >= 0) {
		Start_Flag = 0;
		CircleLine_Flag = 0;
		Crane_Move_Flag = 0;
		Crane_Sucess_Flag = 0;
		Crane_Grab_Flag = 0;
		Selected_Item = 0;
		RTC_Time_Set(0, 0, 0);
		CircleX = 0;
		CircleY = 0;
		Intro_Play();

		if (Coin == 0) {
			DrawImage(buffer, 0, 0, 480, 272, Image_Ending_Bye);
			delay(70000000);
		}
	}
}

void Intro_Play() {
	int Coin_Check = 0;
	DrawImage(buffer, 0, 0, 480, 272, Image_Intro1);

	while (Start_Flag == 0 || Coin == 0) {
		if (Start_Flag != 0)
			Start_Flag = 0;
		Coin_Check = PUSH_Check();

		if (Coin_Check == 1 && Coin < 3) {
			Coin++;
			BUZ_Coin_Add();
		}

		else if (Coin_Check == 2 && Coin > 0) {
			Coin--;
			BUZ_Coin_Sub();
		}

		if (Coin == 0) {
			delay(6000000);
			RestoreBackground(buffer, 0, 0, 480, 272, Image_Intro1);
			delay(6000000);
			RestoreBackground(buffer, 110, 50, 370, 100, Image_Intro2);
		} else if (Coin == 1) {
			delay(6000000);
			RestoreBackground(buffer, 0, 0, 480, 272, Image_Intro_100);
			delay(6000000);
			RestoreBackground(buffer, 75, 100, 400, 140, Image_Intro_Start);
		} else if (Coin == 2) {
			delay(6000000);
			RestoreBackground(buffer, 0, 0, 480, 272, Image_Intro_200);
			delay(6000000);
			RestoreBackground(buffer, 75, 100, 400, 140, Image_Intro_Start);
		} else if (Coin == 3) {
			delay(6000000);
			RestoreBackground(buffer, 0, 0, 480, 272, Image_Intro_300);
			delay(6000000);
			RestoreBackground(buffer, 75, 100, 400, 140, Image_Intro_Start);
		}

		WRITE_FND(5, Coin % 100);

		Coin_Check = 0;
	}
	FND_clear();

	if (Start_Flag == 1 && Coin > 0) {
		Game_Play();
	}
}

void Game_Play() {
	DrawImage(buffer, 0, 0, 480, 272, Image_PlayGround);

	Game_ProductSet();
	CircleLine_Flag = 1;

	Game_CraneMove();
	Game_CraneGrab();
	Ending_Play();

	Coin--;
}

void Game_CraneMove() {
	int Vx1 = 40, Vy1 = 15, Vx2 = 44, Vy2 = 259;
	int Hx1 = 20, Hy1 = 30, Hx2 = 458, Hy2 = 35;
	int PreSec, sec;
	Bx1 = 26;
	By1 = 20;
	Bx2 = 57;
	By2 = 51;

	DrawImage(buffer, Vx1, Vy1, Vx2, Vy2, Image_Crane_VBar);
	DrawImage(buffer, Hx1, Hy1, Hx2, Hy2, Image_Crane_HBar);
	DrawImage(buffer, Bx1, By1, Bx2, By2, Image_Crane_Body);

	Play_Time = 8;

	while (Crane_Move_Flag < 2) {
		code = Uart_GetKey();
		sec = RTC_Time_Out();

		if (PreSec != sec) {
			GPIO_WRITE(GPIO_PORTL, 0Xf, (0xff << Play_Time) & 0x0f);
			GPIO_WRITE(GPIO_PORTM, 0Xf, ((0xff << Play_Time) & 0xf0) >> 4);
			Uart_Printf("%dSec left\r", Play_Time);
			Play_Time--;
			PreSec = sec;
		}

		if (code == 's') {
			Play_Time = 8;
			if (Crane_Move_Flag == 0 && Vx2 < 440) {
				Vx1 += 5;
				Vx2 += 5;
				Bx1 += 5;
				Bx2 += 5;

				RestoreBackground(buffer, Vx1 - 5, Vy1, Vx1, Vy2,
				Image_PlayGround);
				RestoreBackground(buffer, Bx1 - 5, By1, Bx1, By2,
				Image_PlayGround);

				if (Vx2 < 165) {
					DrawImage(buffer, 60, 60, 147, 106,
							product[Item_Sequence[0]].up_image);
					DrawImage(buffer, 60, 126, 147, 172,
							product[Item_Sequence[4]].up_image);
					DrawImage(buffer, 60, 192, 147, 238,
							product[Item_Sequence[8]].up_image);
				} else if (Vx2 < 270) {
					DrawImage(buffer, 161, 60, 248, 106,
							product[Item_Sequence[1]].up_image);
					DrawImage(buffer, 161, 126, 248, 172,
							product[Item_Sequence[5]].up_image);
					DrawImage(buffer, 161, 192, 248, 238,
							product[Item_Sequence[9]].up_image);
				} else if (Vx2 < 370) {
					DrawImage(buffer, 262, 60, 349, 106,
							product[Item_Sequence[2]].up_image);
					DrawImage(buffer, 262, 126, 349, 172,
							product[Item_Sequence[6]].up_image);
					DrawImage(buffer, 262, 192, 349, 238,
							product[Item_Sequence[10]].up_image);
				} else {
					DrawImage(buffer, 363, 60, 450, 106,
							product[Item_Sequence[3]].up_image);
					DrawImage(buffer, 363, 126, 450, 172,
							product[Item_Sequence[7]].up_image);
					DrawImage(buffer, 363, 192, 450, 238,
							product[Item_Sequence[11]].up_image);
				}

				DrawCircleLine(buffer, CircleX, CircleY, 10, COLOR_RED);
				DrawImage(buffer, Vx1, Vy1, Vx2, Vy2, Image_Crane_VBar);
				DrawImage(buffer, Hx1, Hy1, Hx2, Hy2, Image_Crane_HBar);
				DrawImage(buffer, Bx1, By1, Bx2, By2, Image_Crane_Body);
			} else if (Crane_Move_Flag == 1 && Hy2 < 243) {
				Hy1 += 5;
				Hy2 += 5;
				By1 += 5;
				By2 += 5;

				RestoreBackground(buffer, Bx1, By1 - 5, Bx2, By1,
				Image_PlayGround);
				RestoreBackground(buffer, Hx1, Hy1 - 5, Hx2, Hy1,
				Image_PlayGround);

				delay(10000);
				if (Hy2 < 125) {
					DrawImage(buffer, 60, 60, 147, 106,
							product[Item_Sequence[0]].up_image);
					DrawImage(buffer, 161, 60, 248, 106,
							product[Item_Sequence[1]].up_image);
					DrawImage(buffer, 262, 60, 349, 106,
							product[Item_Sequence[2]].up_image);
					DrawImage(buffer, 363, 60, 450, 106,
							product[Item_Sequence[3]].up_image);
				} else if (Hy2 < 200) {
					DrawImage(buffer, 60, 126, 147, 172,
							product[Item_Sequence[4]].up_image);
					DrawImage(buffer, 161, 126, 248, 172,
							product[Item_Sequence[5]].up_image);
					DrawImage(buffer, 262, 126, 349, 172,
							product[Item_Sequence[6]].up_image);
					DrawImage(buffer, 363, 126, 450, 172,
							product[Item_Sequence[7]].up_image);
				} else {
					DrawImage(buffer, 60, 192, 147, 238,
							product[Item_Sequence[8]].up_image);
					DrawImage(buffer, 161, 192, 248, 238,
							product[Item_Sequence[9]].up_image);
					DrawImage(buffer, 262, 192, 349, 238,
							product[Item_Sequence[10]].up_image);
					DrawImage(buffer, 363, 192, 450, 238,
							product[Item_Sequence[11]].up_image);
				}

				DrawCircleLine(buffer, CircleX, CircleY, 10, COLOR_RED);
				DrawImage(buffer, Hx1, Hy1, Hx2, Hy2, Image_Crane_HBar);
				DrawImage(buffer, Vx1, Vy1, Vx2, Vy2, Image_Crane_VBar);
				DrawImage(buffer, Bx1, By1, Bx2, By2, Image_Crane_Body);
			}
		} else if (code == 'a' || Play_Time == -1) {
			BUZ_Move_Confirm();
			Crane_Move_Flag++;
			Play_Time = 8;
		}
	}
	GPIO_WRITE(GPIO_PORTL, 0Xf, 0x0);
	GPIO_WRITE(GPIO_PORTM, 0Xf, 0x0);
}

void Game_CraneGrab() {
	BUZ_Crane_Down();
	Game_CraneGrabPoint();

	if (Crane_Sucess_Flag == 0 && Crane_Grab_Flag == 0) {		// 못잡음
		DrawImage(buffer, 0, 0, 480, 272, Image_Grab_Fail);
		delay(20000000);
	} else if (Crane_Sucess_Flag == 0 && Crane_Grab_Flag == 1) {	// 잡았는데 떨어트림
		DrawImage(buffer, 0, 0, 480, 272, Image_Grab1);
		DrawImage(buffer, 197, 85, 244, 170, product[Selected_Item].side_image);
		delay(10000000);

		DrawImage(buffer, 0, 0, 480, 272, Image_Grab2);
		DrawImage(buffer, 197, 122, 244, 207,
				product[Selected_Item].side_image);
		delay(10000000);

		DrawImage(buffer, 0, 0, 480, 272, Image_Grab_Fail);
		DrawImage(buffer, 197, 10 + 15, 256, 76 + 15,
				product[Selected_Item].drop_image);
		delay(10000000);
	} else {		// 성공
		DrawImage(buffer, 0, 0, 480, 272, Image_Grab1);
		DrawImage(buffer, 197, 85, 244, 170, product[Selected_Item].side_image);
		delay(10000000);

		DrawImage(buffer, 0, 0, 480, 272, Image_Grab2);
		DrawImage(buffer, 197, 122, 244, 207,
				product[Selected_Item].side_image);
		delay(10000000);

		DrawImage(buffer, 0, 0, 480, 272, Image_Grab_Success1);
		DrawImage(buffer, 197 + 100, 122, 244 + 100, 207,
				product[Selected_Item].side_image);
		delay(10000000);

		DrawImage(buffer, 0, 0, 480, 272, Image_Grab_Success2);
		DrawImage(buffer, 197 + 203, 122, 244 + 203, 207,
				product[Selected_Item].side_image);
		BUZ_Crane_Release();
		delay(10000000);
	}
}

void Game_CraneGrabPoint() {
	if (By1 >= 60 && By2 <= 106) {
		if (Bx1 >= 60 && Bx2 <= 147) {
			Crane_Grab_Flag = 1;
			Selected_Item = Item_Sequence[0];
			Crane_Sucess_Flag = Game_Grab_Calculate(
					product[Item_Sequence[0]].success_rate);
		} else if (Bx1 >= 161 && Bx2 <= 248) {
			Crane_Grab_Flag = 1;
			Selected_Item = Item_Sequence[1];
			Crane_Sucess_Flag = Game_Grab_Calculate(
					product[Item_Sequence[1]].success_rate);
		} else if (Bx1 >= 262 && Bx2 <= 349) {
			Crane_Grab_Flag = 1;
			Selected_Item = Item_Sequence[2];
			Crane_Sucess_Flag = Game_Grab_Calculate(
					product[Item_Sequence[2]].success_rate);
		} else if (Bx1 >= 363 && Bx2 <= 450) {
			Crane_Grab_Flag = 1;
			Selected_Item = Item_Sequence[3];
			Crane_Sucess_Flag = Game_Grab_Calculate(
					product[Item_Sequence[3]].success_rate);
		} else {
			Crane_Sucess_Flag = 0;
		}
	} else if (By1 >= 126 && By2 <= 172) {
		if (Bx1 >= 60 && Bx2 <= 147) {
			Crane_Grab_Flag = 1;
			Selected_Item = Item_Sequence[4];
			Crane_Sucess_Flag = Game_Grab_Calculate(
					product[Item_Sequence[4]].success_rate);
		} else if (Bx1 >= 161 && Bx2 <= 248) {
			Crane_Grab_Flag = 1;
			Selected_Item = Item_Sequence[5];
			Crane_Sucess_Flag = Game_Grab_Calculate(
					product[Item_Sequence[5]].success_rate);
		} else if (Bx1 >= 262 && Bx2 <= 349) {
			Crane_Grab_Flag = 1;
			Selected_Item = Item_Sequence[6];
			Crane_Sucess_Flag = Game_Grab_Calculate(
					product[Item_Sequence[6]].success_rate);
		} else if (Bx1 >= 363 && Bx2 <= 450) {
			Crane_Grab_Flag = 1;
			Selected_Item = Item_Sequence[7];
			Crane_Sucess_Flag = Game_Grab_Calculate(
					product[Item_Sequence[7]].success_rate);
		} else {
			Crane_Sucess_Flag = 0;
		}
	} else if (By1 >= 192 && By2 <= 238) {
		if (Bx1 >= 60 && Bx2 <= 147) {
			Crane_Grab_Flag = 1;
			Selected_Item = Item_Sequence[8];
			Crane_Sucess_Flag = Game_Grab_Calculate(
					product[Item_Sequence[8]].success_rate);
		} else if (Bx1 >= 161 && Bx2 <= 248) {
			Crane_Grab_Flag = 1;
			Selected_Item = Item_Sequence[9];
			Crane_Sucess_Flag = Game_Grab_Calculate(
					product[Item_Sequence[9]].success_rate);
		} else if (Bx1 >= 262 && Bx2 <= 349) {
			Crane_Grab_Flag = 1;
			Selected_Item = Item_Sequence[10];
			Crane_Sucess_Flag = Game_Grab_Calculate(
					product[Item_Sequence[10]].success_rate);
		} else if (Bx1 >= 363 && Bx2 <= 450) {
			Crane_Grab_Flag = 1;
			Selected_Item = Item_Sequence[11];
			Crane_Sucess_Flag = Game_Grab_Calculate(
					product[Item_Sequence[11]].success_rate);
		} else {
			Crane_Sucess_Flag = 0;
		}
	} else {
		Crane_Sucess_Flag = 0;
	}
}

int Game_Grab_Calculate(int difficulty) {
	if ((rand() % 100) < difficulty) {
		return 1;
	} else {
		return 0;
	}
}

void Ending_Play() {
	int i;
	char success[15] = "Success!\r\n";
	char failure[15] = "Failure!\r\n";

	if (Crane_Sucess_Flag == 0) {
		DrawImage(buffer, 0, 0, 480, 272, Image_Ending_Failure);

		for (i = 0; i < sizeof(failure); i++) {
			Bluetooth_PutCh(failure[i]);
		}

	} else {
		DrawImage(buffer, 0, 0, 480, 272, Image_Ending_Sucess);
		for (i = 0; i < sizeof(success); i++) {
			Bluetooth_PutCh(success[i]);
		}
	}
	DrawImage(buffer, 45, 30, 395 + 45, 151 + 30,
			product[Selected_Item].info_image);

	for (i = 0; i < sizeof(product[Selected_Item].name); i++) {
		Bluetooth_PutCh(product[Selected_Item].name[i]);
	}

	for (i = 0; i < sizeof(product[Selected_Item].difficulty); i++) {
		Bluetooth_PutCh(product[Selected_Item].difficulty[i]);
	}

	if (Crane_Sucess_Flag == 0) {
		BUZ_Failure();
	} else {
		BUZ_Success();
	}

	delay(90000000);
}

void Game_ProductSet() {
	int i, random;

	srand((unsigned) time(NULL));

	for (i = 0; i < 12; i++) {
		random = rand() % 7 + 1;

		Item_Sequence[i] = random;

		switch (i) {
// ROW 1 BEGIN
		case 0:
			DrawImage(buffer, 60, 60, 147, 106, product[random].up_image);
			break;
		case 1:
			DrawImage(buffer, 161, 60, 248, 106, product[random].up_image);
			break;
		case 2:
			DrawImage(buffer, 262, 60, 349, 106, product[random].up_image);
			break;
		case 3:
			DrawImage(buffer, 363, 60, 450, 106, product[random].up_image);
			break;
			// ROW 1 END
			// ROW 2 BEGIN
		case 4:
			DrawImage(buffer, 60, 126, 147, 172, product[random].up_image);
			break;
		case 5:
			DrawImage(buffer, 161, 126, 248, 172, product[random].up_image);
			break;
		case 6:
			DrawImage(buffer, 262, 126, 349, 172, product[random].up_image);
			break;
		case 7:
			DrawImage(buffer, 363, 126, 450, 172, product[random].up_image);
			break;
			// ROW 2 END
			// ROW 3 BEGIN
		case 8:
			DrawImage(buffer, 60, 192, 147, 238, product[random].up_image);
			break;
		case 9:
			DrawImage(buffer, 161, 192, 248, 238, product[random].up_image);
			break;
		case 10:
			DrawImage(buffer, 262, 192, 349, 238, product[random].up_image);
			break;
		case 11:
			DrawImage(buffer, 363, 192, 450, 238, product[random].up_image);
			break;
			// ROW 3 END
		default:
			break;
		}
	}
}

void BUZ_Coin_Add() {
	GPIO_WRITE(GPIO_PORTD, 0x10, 0x10);
	Play(NONE);
	DelayForPlay(DLY_32);
	Play(C2);
	DelayForPlay(DLY_16);
	Play(High);
	DelayForPlay(DLY_16);
	GPIO_WRITE(GPIO_PORTD, 0x10, ~0x10);
	WDT1LOAD = 0xFFFFFFFF;
}

void BUZ_Coin_Sub() {
	GPIO_WRITE(GPIO_PORTD, 0x10, 0x10);
	Play(NONE);
	DelayForPlay(DLY_32);
	Play(High);
	DelayForPlay(DLY_16);
	Play(C2);
	DelayForPlay(DLY_16);
	GPIO_WRITE(GPIO_PORTD, 0x10, ~0x10);
	WDT1LOAD = 0xFFFFFFFF;
}

void BUZ_Move_Confirm() {
	GPIO_WRITE(GPIO_PORTD, 0x10, 0x10);
	Play(NONE);
	DelayForPlay(DLY_32);
	Play(G1);
	DelayForPlay(DLY_16);
	GPIO_WRITE(GPIO_PORTD, 0x10, ~0x10);
	WDT1LOAD = 0xFFFFFFFF;
}

void BUZ_Crane_Down() {
	GPIO_WRITE(GPIO_PORTD, 0x10, 0x10);
	Play(NONE);
	DelayForPlay(DLY_32);
	Play(High);
	DelayForPlay(DLY_32);
	Play(High);
	DelayForPlay(DLY_32);
	Play(High);
	DelayForPlay(DLY_32);
	Play(High);
	DelayForPlay(DLY_32);
	GPIO_WRITE(GPIO_PORTD, 0x10, ~0x10);
	WDT1LOAD = 0xFFFFFFFF;
}

void BUZ_Crane_Release() {
	GPIO_WRITE(GPIO_PORTD, 0x10, 0x10);
	Play(NONE);
	DelayForPlay(DLY_32);
	Play(65000);
	DelayForPlay(DLY_16);
	Play(65000);
	DelayForPlay(DLY_16);
	GPIO_WRITE(GPIO_PORTD, 0x10, ~0x10);
	WDT1LOAD = 0xFFFFFFFF;
}

void BUZ_Success() {
	GPIO_WRITE(GPIO_PORTD, 0x10, 0x10);
	Play(NONE);
	DelayForPlay(DLY_32);
	Play(C1);
	DelayForPlay(DLY_16);
	Play(E1);
	DelayForPlay(DLY_16);
	Play(G1);
	DelayForPlay(DLY_16);
	Play(C2);
	DelayForPlay(DLY_8);
	GPIO_WRITE(GPIO_PORTD, 0x10, ~0x10);
	WDT1LOAD = 0xFFFFFFFF;
}
void BUZ_Failure() {
	GPIO_WRITE(GPIO_PORTD, 0x10, 0x10);
	Play(NONE);
	DelayForPlay(DLY_32);
	Play(D1);
	DelayForPlay(DLY_16);
	Play(C1);
	DelayForPlay(DLY_16);
	Play(D1);
	DelayForPlay(DLY_16);
	Play(65000);
	DelayForPlay(DLY_8);
	GPIO_WRITE(GPIO_PORTD, 0x10, ~0x10);
	WDT1LOAD = 0xFFFFFFFF;
}

int32_t PointerMessage(uint32_t ui32Message, int32_t i32X, int32_t i32Y) {
	if (Start_Flag == 0) {
		Start_Flag = 1;
	}

	else if (Start_Flag == 1 && CircleLine_Flag == 1) {
		if (user_X >= 0 && user_X < 480 && user_Y >= 0 && user_Y < 272) {
			CircleX = user_X;
			CircleY = user_Y;
			DrawCircleLine(buffer, user_X, user_Y, 10, COLOR_RED);
			CircleLine_Flag = 2;
		}
	}

	return 0;
}

void _user_Bluetooth_Interrupt_Handler(void) {
	UART3IM = UART3IM & ~(0x1 << 4);

	code = Bluetooth_GetKey();

	UART3IM = UART3IM | (0x1 << 4);
	UART3ICR = UART3ICR | (0x1 << 4);
	INTUNPEND1 = INTPEND1;
}
